<?php

// Routes

$app->get('/hello{a:/{0,1}}{name:[\w]*}', function(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
    $name = $args['name'];
    $name = htmlspecialchars($name);
    return $response->write("Hello $name");
});

$app->get('/', 'App\Action\HomeAction:dispatch')->setName('homepage');


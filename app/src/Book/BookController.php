<?php

namespace App\Book;

//use App\CrudBaseController as CrudBaseController;

class BookController extends \App\Base\RestBaseController {

    protected function getRouteBase() {
        return 'books';
    }

    public function index(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        return $response->write("Displaying all books - index");
    }

    public function create(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        return $response->write("Add new book - create");
    }

    public function store(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        return $response->write("store new book - store");
    }
    
    public function show(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        return $response->write("Display a single book - show");
    }
    
    public function edit(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        return $response->write("Edit a single book - edit");
    }

    public function update(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        return $response->write("update a single book - update");
    }

    public function delete(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        return $response->write("delete a book - delete");
    }

}

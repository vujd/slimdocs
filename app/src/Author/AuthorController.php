<?php

namespace App\Author;

use Slim\Http\Response as Response;
use Slim\Http\Request as Request;

class AuthorController extends \App\Base\BaseCrudController {

    protected static function getRouteBase() {
        return '/authors';
    }

    protected static function registerRoutes($app) {
        parent::registerRoutes($app);
        static::addRoute($app, ['GET', 'POST'], '/delete{a:/{0,1}}{id:[\d]*}', 'delete');
    }

    public function add(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        return $response->write("Add new Author - add method");
    }

    public function delete(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
         $id = '';
        if (isset($args['id']))
            $id = $args['id'];
        
        $link = $this->pathFor('authors-delete', ['id' => $id, 'a'=>'']);
        $response->write("editing  Author - id:{$id}</br>");

        return $response->write("with the link {$link}");
    }

    public function edit(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        $id = '';
        if (isset($args['id']))
            $id = $args['id'];

        $link = $this->pathFor('authors-edit', ['id' => $id]);
        $response->write("editing  Author - id:{$id}</br>");

        return $response->write("with the link {$link}");
    }

    public function index(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        return $response->write("Displaying all authors - index");
    }

    public function show(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        return $response->write("Displaying a single author - show");
    }

}

<?php

namespace App\Publisher;

class PublisherController extends \App\Base\BaseCrudController {

    protected static function getRouteBase() {
        return "/publishers";
    }

    protected static function registerRoutes($app) {
        parent::registerRoutes($app);
        static::addRoute($app, ['GET', 'POST'], '/login', 'login');
    }

    public function index(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        $link = $this->pathFor('publishers-index');
        $response->write("Displaying all publishers - index");
        return $response->write("make link for:  {$link}");
    }
    public function login(\Slim\Http\Request $request, \Slim\Http\Response $response, $args) {
        $link = $this->pathFor('publishers-edit', ['id'=>123]);
        $response->write("Login publishers - index</br>");
        return $response->write("make link for edit:  {$link}");
    }

}

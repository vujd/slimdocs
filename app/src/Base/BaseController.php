<?php

namespace App\Base;

abstract class BaseController {

    protected $app;
    protected $view = null;
    protected $logger = null;
    protected $flash = null;
    
    public function __construct(\Slim\App $app) {
        $this->app = $app;

        if ($app->getContainer()->has('logger')) {
            $this->logger = $app->logger;
        }

        if ($app->getContainer()->has('flash')) {
            $this->flash = $app->flash;
        }

        if ($app->getContainer()->has('view')) {
            $this->view = $app->view;
        }
    }

    //Wrapper methods for convienience

    public function pathFor($name, array $data = [], array $queryParams = []) {
        return $this->app->router->pathFor($name, $data, $queryParams);
    }

    public function addMessage($key, $message) {
        if (isset($this->flush)) {
            $this->flash->addMessage($key, $message);
        }
    }

    public function getMessages() {
        if (isset($this->flush)) {
            return $this->flash->getMessages();
        }
    }

    public function getMessage($key) {
        if (isset($this->flush)) {
            return $this->flash->getMessage($key);
        }
    }

}

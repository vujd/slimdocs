<?php

namespace App\Base;

use Slim\Http\Response as Response;
use Slim\Http\Request as Request;

abstract class RestBaseController extends BaseController {
    private $pattern = '\\';
   
    public function __construct($app) {
        parent::__construct($app);
    }

    protected function setRoutes() {

        $base = $this->getRouteBase();
        
        $this->app->get('/' . $base , [$this, 'index'])->setName($base . '-all');
        //New item
        $this->app->get('/' . $base . '/create', [$this, 'create'])->setName($base . '-create');
        $this->app->post('/' . $base , [$this, 'store'])->setName($base . '-store');
        //edit item
        $this->app->get('/' . $base . "/{id:[\d]*}", [$this, 'show'])->setName($base . '-show');
        $this->app->get('/' . $base . '/{id:[\d]*}/edit', [$this, 'edit'])->setName($base . '-edit');
        $this->app->put('/' . $base . '/{id:[\d]*}', [$this, 'update'])->setName($base . '-update');
        //delete
        $this->app->delete('/' . $base . '/{id:[\d]*}', [$this, 'delete'])->setName($base . '-delete');
    }

    public function index(Request $request, Response $response, $args) {
        $f = $this->app->getContainer()->get('notFoundHandler');
        return $f($request, $response);
    }

    public function create(Request $request, Response $response, $args) {
        $f = $this->app->getContainer()->get('notFoundHandler');
        return $f($request, $response);
    }

    public function store(Request $request, Response $response, $args) {
        $f = $this->app->getContainer()->get('notFoundHandler');
        return $f($request, $response);
    }

    public function show(Request $request, Response $response, $args) {
        //by default show and edit are the same
        return $this->edit($request, $response, $args);
    }

    public function edit(Request $request, Response $response, $args) {
        $c = $this->app->getContainer();
        return $c['notFoundHandler']($request, $response);
    }

    public function update(Request $request, Response $response, $args) {
        $c = $this->app->getContainer();
        return $c['notFoundHandler']($request, $response);
    }

    public function delete(Request $request, Response $response, $args) {
        $c = $this->app->getContainer();
        return $c['notFoundHandler']($request, $response);
    }

    public function getRouteName($action) {
        return "{$this->getRouteBase()}-{$action}";
    }

    
}

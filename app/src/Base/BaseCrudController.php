<?php

namespace App\Base;

use Slim\Http\Response as Response;
use Slim\Http\Request as Request;

abstract class BaseCrudController extends BaseRouteController {

    protected static function registerRoutes($app) {
        $class_name = get_called_class(); 
        $base = static::getRouteBase();
        $name = str_replace('/', '', $base);
        
        $app->get($base, $class_name . ':index')->setName($name . '-index');
             
        static::addRoute($app, ['GET', 'POST'], '/add/', 'add');   
        static::addRoute($app, ['GET', 'POST'], '/edit/[{id:[\d]*}]', 'edit'); 
        static::addRoute($app, ['GET'], '/show/[{id:[\d]*}]', 'show'); 
    }
    
    public function index(Request $request, Response $response, $args) {
        $f = $this->app->getContainer()->get('notFoundHandler');
        return $f($request, $response);
    }

    public function show(Request $request, Response $response, $args) {
        $f = $this->app->getContainer()->get('notFoundHandler');
        return $f($request, $response);
    }

    public function add(Request $request, Response $response, $args) {
        $f = $this->app->getContainer()->get('notFoundHandler');
        return $f($request, $response);
    }

    public function edit(Request $request, Response $response, $args) {
        $c = $this->app->getContainer();
        return $c['notFoundHandler']($request, $response);
    }

    public function delete(Request $request, Response $response, $args) {
        $f = $this->app->getContainer()->get('notFoundHandler');
        return $f($request, $response);
    }

   
}

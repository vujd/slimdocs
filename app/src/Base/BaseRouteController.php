<?php

namespace App\Base;

use Slim\Http\Response as Response;
use Slim\Http\Request as Request;

abstract class BaseRouteController extends BaseController{
    
    abstract  protected static function registerRoutes($app);
    
    public static function register($app) {
        $class_name = get_called_class();
        $container = $app->getContainer();

        $container[$class_name] = function ($container) use($app, $class_name) {
            return new $class_name($app);
        };
        static::registerRoutes($app);
    }
    
    public static function addRoute(\Slim\App $app, array $methods, $pattern, $action, $name = null) {
        $class_name = get_called_class();
        $base = static::getRouteBase();

        if ($name === null)
            $name = str_replace('/', '-', substr($base, 1)) . "-" . $action;
           
        $app->map($methods, $base . $pattern, $class_name . ':' . $action)->setName($name);
    }
 
    abstract protected static function getRouteBase();
    
    public function __construct(\Slim\App $app) {
        parent::__construct($app);
    }

    

}

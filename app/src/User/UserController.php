<?php

namespace App\User;

final class UserController extends \App\Base\BaseRouteController{

    protected static function getRouteBase() {
       return ''; 
    }
    
    protected static function registerRoutes($app) {
        $app->get('/user', 'App\\User\\UserController:index')->setName('user');
        $app->get('/auser', 'App\\User\\UserController:auser')->setName('auser');
    }
   
    public function index($request, $response, $args) {
        $this->logger->info("User page action dispatched");
        $this->view->render($response, 'user.twig');
        return $response;
    }

    public function auser($request, $response, $args) {
        $this->logger->info("A-User page action dispatched");
        return $response->withRedirect($this->pathFor('user'));
    }

}
